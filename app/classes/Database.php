<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/24/2020
 * Time: 8:52 AM
 */
namespace App\classes;
class Database
{
    public function dbConnect () {
        $hostName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'blog';
        $link = mysqli_connect($hostName, $userName, $password, $database);
        return $link;
    }

}