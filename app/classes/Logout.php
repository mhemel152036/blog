<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/24/2020
 * Time: 11:03 AM
 */
namespace App\classes;
class Logout
{
    public function adminLogOut () {
        unset($_SESSION['id']);
        unset($_SESSION['name']);
        header('Location: index.php');
    }

}