<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/26/2020
 * Time: 10:55 AM
 */

namespace App\classes;


class Blog
{
    public function addBlog ($data) {
        $message =[];
        $upload = false;
        $dir = "../assets/image/";
        $imgName = $_FILES['blog_img']['name'];
        $imgUrl = $dir.$imgName;
        if(!file_exists($imgUrl)) {
            if (getimagesize($_FILES['blog_img']['tmp_name'])) {
                $imgType = pathinfo($imgUrl, PATHINFO_EXTENSION);

                if ($imgType === 'jpg' || $imgType === 'JPG' || $imgType === 'JPEG' || $imgType === 'jpeg') {
                    if ($_FILES['blog_img']['size'] > 5000000) {
                        $message = [
                            "negative" => "File size is more than 5MB!"
                        ];
                        return $message;
                    }
                    else {
                        $upload = true;
                    }

                }
                else {
                    $message = [
                        "negative" => "Not a valid image type!"
                    ];
                    return $message;
                }
            }
            else {
                $message = [
                    "negative" => "Not a image file!"
                ];
                return $message;
            }
        }
        else{
            $message = [
                "negative" => "File already exists in the database!"
            ];
            return $message;
        }

        if($upload==true){
            $link = Database::dbConnect();
            $sql = "INSERT INTO blogs (category_id,blog_title,short_description,long_description,blog_image,publication_status) VALUES ('$data[category_id]','$data[blog_title]','$data[short_description]','$data[long_description]','$imgUrl','$data[publication_status]')";
            if (mysqli_query($link, $sql)){
                move_uploaded_file($_FILES['blog_img']['tmp_name'],$imgUrl);
                $message['positive']="Blog post added successfully!";
                return $message;
            }
            else{
                die("Query problem: ".mysqli_error($link));
            }
        }

    }
    public function getBlogs(){
        $link = Database::dbConnect();
        $sql = "SELECT b.*,c.category_name FROM blogs AS b JOIN categories as c ON b.category_id=c.category_id";
        if(mysqli_query($link, $sql)){
            $queryResult = mysqli_query($link, $sql);
            return $queryResult;
        }
        else{
            die("Query probme: ".mysqli_error($link));
        }

    }
    public function deleteBlogById($id){
        $link = Database::dbConnect();
        $sql = "SELECT blog_image FROM blogs WHERE id='$id'";
        if(mysqli_query($link, $sql)){
            $oldBlog = mysqli_fetch_assoc(mysqli_query($link,$sql));
            if(file_exists($oldBlog['blog_image'])){
                unlink($oldBlog['blog_image']);
            }
        }
        else{
            die("Query problem: ".mysqli_error($link));
        }

        $sql = "DELETE FROM blogs WHERE id='$id'";
        if(mysqli_query($link,$sql)){
            $message = "Blog deleted successfully!";
            return $message;
        }
        else{
            die("Query problem: ".mysqli_error($link));
        }

    }
    public function getBlogById($id){
        $link = Database::dbConnect();
        $sql = "SELECT * FROM blogs WHERE id='$id'";
        if(mysqli_query($link,$sql)){
            $queryResult = mysqli_query($link,$sql);
            $blog = mysqli_fetch_assoc($queryResult);
            return $blog;
        }else{
            die("Query problem: ".mysqli_error($link));
        }
    }
    public function updateBlog($data){
//
//
//        if(mysqli_query($link,$sql)){
//            $message = "Blogpost updated successfully!";
//            return $message;
//        }else{
//            die("Query problem: ".mysqli_error($link));
//        }

        $message =[];
        $link = Database::dbConnect();
        if($_FILES['blog_img']['name']){
            $upload = false;
            $dir = "../assets/image/";
            $imgName = $_FILES['blog_img']['name'];
            $imgUrl = $dir.$imgName;
            if(!file_exists($imgUrl)) {
                if (getimagesize($_FILES['blog_img']['tmp_name'])) {
                    $imgType = pathinfo($imgUrl, PATHINFO_EXTENSION);

                    if ($imgType === 'jpg' || $imgType === 'JPG' || $imgType === 'JPEG' || $imgType === 'jpeg') {
                        if ($_FILES['blog_img']['size'] > 5000000) {
                            $message = [
                                "negative" => "File size is more than 5MB!"
                            ];
                            return $message;
                        }
                        else {
                            $upload = true;
                        }

                    }
                    else {
                        $message = [
                            "negative" => "Not a valid image type!"
                        ];
                        return $message;
                    }
                }
                else {
                    $message = [
                        "negative" => "Not a image file!"
                    ];
                    return $message;
                }
            }
            else{
                $message = [
                    "negative" => "File already exists in the database!"
                ];
                return $message;
            }

            if($upload==true){
                /*delete existing old file*/
                $sql = "SELECT blog_image FROM blogs WHERE id='$data[blog_id]'";
                if(mysqli_query($link, $sql)){
                    $oldBlog=mysqli_fetch_assoc(mysqli_query($link,$sql));
                    if(file_exists($oldBlog['blog_image'])){
                        unlink($oldBlog['blog_image']);
                    }
                } else{
                    die("Query problem ".mysqli_error($link));
                }


                $sql = "UPDATE blogs SET category_id='$data[category_id]', blog_title='$data[blog_title]', short_description='$data[short_description]', long_description='$data[long_description]', blog_image='$imgUrl', publication_status='$data[publication_status]' WHERE id='$data[blog_id]'";

                if (mysqli_query($link, $sql)){
                    move_uploaded_file($_FILES['blog_img']['tmp_name'],$imgUrl);
                    $message['positive']="Blog post added successfully!";
                    return $message;
                }
                else{
                    die("Query problem: ".mysqli_error($link));
                }
            }
        } else{
            $sql = "UPDATE blogs SET category_id='$data[category_id]', blog_title='$data[blog_title]', short_description='$data[short_description]', long_description='$data[long_description]' ,publication_status='$data[publication_status]' WHERE id='$data[blog_id]'";

            if (mysqli_query($link, $sql)){
                $message['positive']="Blog post added successfully!";
                return $message;
            } else{
                die("Query problem: ".mysqli_error($link));
            }
        }
    }

}