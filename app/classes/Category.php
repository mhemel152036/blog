<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/26/2020
 * Time: 10:56 AM
 */

namespace App\classes;

use App\classes\Database;
class Category
{
    public function addCategory ($data) {
        $link = Database::dbConnect();
        $sql = "INSERT INTO categories (category_name,category_description,publication_status) VALUES ('$data[category_name]','$data[category_description]','$data[publication_status]')";
        if(mysqli_query($link,$sql)){
            $message = "Blogpost added successfully!";
            return $message;
        }else{
            die("Query problem ".mysqli_error($link));
        }
    }
    public function getCategories () {
        $link = Database::dbConnect();
        $sql = "SELECT * FROM categories WHERE 1";
        if(mysqli_query($link, $sql)) {
            $queryResult = mysqli_query($link,$sql);
            return $queryResult;
        }else{
            die("Query problem ".mysqli_error($link));
        }
    }
    public function getPublishedCategories() {
        $link = Database::dbConnect();
        $sql = "SELECT category_id,category_name FROM categories WHERE publication_status = '1'";
        if(mysqli_query($link, $sql)) {
            $queryResult = mysqli_query($link, $sql);
            return $queryResult;
        }else{
            die("Query problem ".mysqli_error($link));
        }
    }
    public function deleteCategoryById ($id){
        $link = Database::dbConnect();
        $sql = "DELETE FROM categories WHERE category_id='$id'";
        if(mysqli_query($link,$sql)){
            $message = "Category deleted successfully";
            return $message;
        }
        else{
            die("Query problem ".mysqli_error($link));
        }
    }
    public function getCategoryById($id) {
        $link = Database::dbConnect();
        $sql = "SELECT * FROM categories WHERE  category_id='$id'";
        if(mysqli_query($link, $sql)){
            $queryResult = mysqli_query($link, $sql);
            $category = mysqli_fetch_assoc($queryResult);
            return $category;
        }else{
            die("Query problem: ".mysqli_error($link));
        }
    }
    public function updateCategory($data){
        $id = $data['category_id'];
        $link = Database::dbConnect();
        $sql = "UPDATE categories SET category_name='$data[category_name]', category_description='$data[category_description]', publication_status='$data[publication_status]' WHERE category_id='$id'";
        if(mysqli_query($link,$sql)){
            $message = "Category information udpated successfully!";
            return $message;
        }else{
            die('Query problem: '.mysqli_error($link));
        }
    }


}