<?php
    require_once "vendor/autoload.php";

    $application = new \App\classes\Application();
    $blogs = $application->getPublishedBlogs();
    $blogs2 = $application->getPublishedBlogs();


    if(isset($_GET['id'])) {
        $id = $_GET['id'];
        $blog = $application->getBlogById($id);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="assets/css/style1.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="">BlogSite</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="" data-toggle="modal" data-target="#myModal">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="admin/">Login</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron mb-0">
        <div class="container">
            <div id="slider1" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php $i=0; while($item = mysqli_fetch_assoc($blogs)) { if($i==0){  ?>
                        <div class="carousel-item active">
                            <img style="height: 500px; width: 1100px;" src="admin/<?php echo $item['blog_image']; ?>" class="d-block img-fluid" alt="<?php echo "Slide".$i; ?>">

                            <div class="carousel-caption">
                                <h4><?php echo $item['blog_title']; ?></h4>
                                <p><?php echo $item['short_description']; ?></p>
                            </div>
                        </div>
                    <?php }else{ $i=$i++; ?>
                        <div class="carousel-item">
                        <img style="height: 500px; width: 1100px;" src="admin/<?php echo $item['blog_image']; ?>" class=" d-block img-fluid" alt="<?php echo "Slide".$i; ?>">

                            <div class="carousel-caption">
                                <h4><?php echo $item['blog_title']; ?></h4>
                                <p><?php echo $item['short_description']; ?></p>
                            </div>

                        </div>
                    <?php  } $i++; if($i>=9){ break;} }?>

                    <a href="#slider1" class="carousel-control-prev" data-slide="prev"><span class="carousel-control-prev-icon"></span></a>
                    <a href="#slider1" class="carousel-control-next" data-slide="next"><span class="carousel-control-next-icon"></span></a>
                </div>
            </div>
        </div>
        </div>

<!--        <div class="jumbotron">-->
        <!-- Page Content -->
        <div class="jumbotron mb-1">
        <div class="container">

            <!-- Jumbotron Header -->




            <!-- Page Features -->
            <div class="row text-justify">
                <?php while ($blogInfo = mysqli_fetch_assoc($blogs2)) { ?>

                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <img height="180px" class="card-img-top" src="app/<?php echo $blogInfo['blog_image']; ?>" alt="<?php echo $blogInfo['blog_image']; ?>">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $blogInfo['blog_title']; ?></h4>
                            <p class="card-text"><?php echo $blogInfo['short_description']; ?></p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="" class="btn btn-primary text-light" data-toggle="modal" data-target="<?php echo "#id".$blogInfo['id']; ?>">Read more...</a>
                        </div>
                    </div>
                </div>

                    <div class="modal" id="<?php echo "id".$blogInfo['id']; ?>">
                        <div class="modal-dialog modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header bg-light">
                                    <h5 class="modal-title"><?php echo $blogInfo['blog_title'];?></h5>
                                    <button class="close" data-dismiss="modal" data-target="<?php echo "#id".$blogInfo['id']; ?>"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <img class="card-img" src="admin/<?php echo $blogInfo['blog_image']; ?>" alt="<?php echo $blogInfo['blog_image']; ?>"/>
                                    <hr>
                                    <p class="card-text lead"><?php echo $blogInfo['short_description']; ?></p>
                                    <hr>
                                    <p class="card-text"><?php echo $blogInfo['long_description']; ?></p>
                                </div>
                                <div class="modal-footer bg-light">
                                    <button class="btn btn-warning" data-dismiss="modal" data-target="<?php echo "#id".$blogInfo['id']; ?>">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        </div>


<!--        Contact Modal-->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Contact</h5>
                        <button class="close" data-toggle="modal" data-target="#myModal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <!--                <div class="card">-->
                        <!--                    <div class="card-body">-->
                        <form action="" method="POST">
                            <div class="row form-group">
                                <label class="col-sm-3" for="name">Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" id="name" name="name"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3" for="email">Email</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="email" id="email" name="email"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3" for="message">Message</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="message" id="message" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">

                                </div>
                                <div class="col-sm-9">
                                    <input class="btn btn-warning" type="submit" name="btn" value="Send Message"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--            </div>-->
                <!--        </div>-->
            </div>
        </div>





        <!-- Footer -->
        <footer class="py-5 mt-2 bg-dark">
            <div class="container text-center text-white">
                <h3>BlogSite</h3>
                <p class="m-0 text-white">Copyright &copy; 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
