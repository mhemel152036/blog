<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/28/2020
 * Time: 4:18 PM
 */
require_once "../vendor/autoload.php";
include_once "session-check/checkId.php";

$blog = new \App\classes\Blog();
$category = new \App\classes\Category();

if(isset($_GET['blog_id'])){
    $id = $_GET['blog_id'];
    $blogInfo = $blog->getBlogById($id);
    $queryResult = $category->getPublishedCategories();
}else{
    header('Location: manage-blog.php');
}

if(isset($_POST['btn'])){
    $message = $blog->updateBlog($_POST);
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Update blog</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/css/style1.css"/>
</head>
<body>

<?php include "navbar/nav-menu.php"; ?>

<div class="jumbotron">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="text-center mt-0 mb-4">Update Blog</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if(isset($message['positive'])){ ?>
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <strong>Success! </strong> <?php echo $message['positive']; ?>
                                    </div>
                                <?php } if(isset($message['negative'])) { ?>
                                    <div class="alert alert-warning alert-dismissible fade show">
                                        <buttton class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </buttton>
                                        <strong>Warning! </strong> <?php echo $message['negative']; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <form name="editBlogForm" action="" method="POST" enctype="multipart/form-data">
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="blog-id">Blog Id</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input readonly class="form-control" type="text" value="<?php echo $blogInfo['id']; ?>" name="blog_id" id="blog-id"/>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="category-id">Category Name</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <select class="form-control" name="category_id" id="category-id">
                                        <option value="">-- Select Category Name --</option>
                                        <?php while ($categoryInfo = mysqli_fetch_assoc($queryResult)) { if($categoryInfo['category_id'] == $blogInfo['category_id']) { ?>
                                            <option selected value="<?php echo $categoryInfo['category_id'] ?>"> <?php echo $categoryInfo['category_name'];?> </option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $categoryInfo['category_id'] ?>"> <?php echo $categoryInfo['category_name'];?> </option>
                                        <?php }} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="blog-title">Blog Title</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="form-control" type="text" name="blog_title" id="blog-title" value="<?php echo $blogInfo['blog_title']; ?>"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="shot-description">Short Description</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <textarea class="form-control" name="short_description" id="shot-description" cols="30" rows="3"><?php echo $blogInfo['short_description']; ?></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="long-description">Long Description</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <textarea class="form-control" name="long_description" id="long-description" cols="30" rows="5"><?php echo $blogInfo['long_description']; ?></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="blog-img">Blog Image</label>
                                <div class="col-sm-12 col-md-4 col-xl-6">
                                    <input class="form-control-file" type="file" name="blog_img" id="blog-img"/>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-3">
                                    <img width="100" src="<?php echo $blogInfo['blog_image']; ?>" alt="<?php echo $blogInfo['blog_image']; ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3">Publication status</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <?php if($blogInfo['publication_status']==1){ ?>
                                        <input class="mr-2" type="radio" name="publication_status" checked value="1"/>Published
                                        <input class="mx-2" type="radio" name="publication_status" value="0"/>Unpublished
                                    <?php } else{?>
                                        <input class="mr-2" type="radio" name="publication_status" value="1"/>Published
                                        <input class="mx-2" type="radio" name="publication_status" checked value="0"/>Unpublished
                                    <?php }?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12 col-md-4 col-xl-3"></div>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="btn btn-success btn-block" type="submit" name="btn" value="Update-Blog">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--    Footer section-->
<?php include_once "footer/footer.php"; ?>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</body>
</html>
