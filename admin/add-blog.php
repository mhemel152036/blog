<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 6:16 PM
 */
require_once "../vendor/autoload.php";
include_once "session-check/checkId.php";
$category = new \App\classes\Category();
$queryResult = $category->getPublishedCategories();

if(isset($_POST['btn'])){
    $blog = new \App\classes\Blog();
    $message = $blog->addBlog($_POST);
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Add blog</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/css/style1.css"/>
</head>
<body>

    <?php include "navbar/nav-menu.php"; ?>

    <div class="jumbotron">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="text-center mt-0 mb-4">Add Blog</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if(isset($message['positive'])){ ?>
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <strong>Success! </strong> <?php echo $message['positive']; ?>
                                    </div>
                                <?php } if(isset($message['negative'])) { ?>
                                    <div class="alert alert-warning alert-dismissible fade show">
                                        <buttton class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </buttton>
                                        <strong>Warning! </strong> <?php echo $message['negative']; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <form action="" method="POST" enctype="multipart/form-data">
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="category-id">Category Name</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <select class="form-control" name="category_id" id="category-id">
                                        <option value="">-- Select Category Name --</option>
                                        <?php while ($categoryData = mysqli_fetch_assoc($queryResult)) { ?>
                                            <option value="<?php echo $categoryData['category_id'] ?>"> <?php echo $categoryData['category_name'];?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="blog-title">Blog Title</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="form-control" type="text" name="blog_title" id="blog-title"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="shot-description">Short Description</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <textarea class="form-control" name="short_description" id="shot-description" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="long-description">Long Description</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <textarea class="form-control" name="long_description" id="long-description" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3" for="blog-img">Blog Image</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="form-control-file" type="file" name="blog_img" id="blog-img"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-12 col-md-4 col-xl-3">Publication status</label>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="mr-2" type="radio" name="publication_status" value="1"/>Published
                                    <input class="mx-2" type="radio" name="publication_status" value="0"/>Unpublished
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12 col-md-4 col-xl-3">

                                </div>
                                <div class="col-sm-12 col-md-8 col-xl-9">
                                    <input class="btn btn-success btn-block" type="submit" name="btn" value="Add-Blog">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!--    Footer section-->
    <?php include_once "footer/footer.php"; ?>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>

<!--    tinymce plugin -->
    <script src="../assets/plugin/tinymce/js/tinymce/tinymce.min.js"></script>
<!--    plugin initilizer -->
    <script>
        tinymce.init({ selector:'textarea' });
    </script>

    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>

