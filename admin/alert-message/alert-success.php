<div class="row">
    <?php if(isset($message)) { ?>
        <div class="col-sm-12 alert alert-success alert-dismissible fade show">
            <button class="close" data-dismiss="alert"><span>&times;</span></button>
            <strong>Success! </strong> <?php echo $message ; ?>
        </div>
    <?php }?>
</div>