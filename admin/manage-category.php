<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 5:34 PM
 */
require_once "../vendor/autoload.php";
include_once "session-check/checkId.php";

$category = new \App\classes\Category();
$queryResult = $category->getCategories();
$queryResultInsideSlider = $queryResult;

if (isset($_GET['delete'])){
    $id = $_GET['category_id'];
    $message = $category->deleteCategoryById($id);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Manage category</title>
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="../assets/css/style1.css"/>
    </head>
    <body>

    <?php include_once "navbar/nav-menu.php"; ?>

    <section id="manageCategorySection">
        <div class="jumbotron">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <?php include_once "alert-message/alert-success.php"; ?>

                                <table class="table table-hover table-responsive-lg table-bordered table-striped">
                                    <thead class="bg-info text-light">
                                        <tr>
                                            <th>No.</th>
                                            <th>Category Name</th>
                                            <th>Category Description</th>
                                            <th>Publication status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count = 1; while ($categoryInfo = mysqli_fetch_assoc($queryResult)) { ?>
                                            <tr>
                                                <th><?php echo $count++; ?></th>
                                                <td><?php echo $categoryInfo['category_name']; ?></td>
                                                <td><?php echo $categoryInfo['category_description']; ?></td>
                                                <td><?php echo $categoryInfo['publication_status'] ? "Published" : "Unpublished"; ?></td>
                                                <td>
                                                    <a class="btn btn-primary" href="update-category.php?category_id=<?php echo $categoryInfo['category_id']; ?>">Update</a>
                                                    <a class="btn btn-danger" onclick="return confirm('Are you sure to delete category?');" href="?delete=true&category_id=<?php echo $categoryInfo['category_id']; ?>">Delete</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include_once "footer/footer.php"; ?>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    </body>
</html>

