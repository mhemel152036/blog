<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 4:52 PM
 */
require_once '../vendor/autoload.php';
include_once "session-check/checkId.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="../assets/css/style1.css"/>
    </head>
    <body>
        <?php include "navbar/nav-menu.php"; ?>
        <section class="dashboard-section">

        </section>
        <?php include_once "footer/footer.php"; ?>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
