<?php
    $logout = new \App\classes\Logout();
    if(isset($_GET['logout'])){
        $logout->adminLogOut();
    }
?>
<nav class="navbar navbar-dark bg-primary navbar-expand-md mt-1 sticky-top">
    <div class="container">
        <a class="navbar-brand" href="dashboard.php">BlogSite</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item"><a class="nav-link" href="dashboard.php">Home</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Category</a>
                    <div class="dropdown-menu">
                        <a href="add-category.php" class="dropdown-item">Add category</a>
                        <a href="manage-category.php" class="dropdown-item">Manage category</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Blog</a>
                    <div class="dropdown-menu">
                        <a href="add-blog.php" class="dropdown-item">Add Blog</a>
                        <a href="manage-blog.php" class="dropdown-item">Manage Blog</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="" class="nav-link dropdown-toggle" data-toggle="dropdown">Super Admin</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="">Profile</a>
                        <a class="dropdown-item" href="?logout=true">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
        <!--                <form class="mr-auto" action="" method="GET">-->
        <!--                    <div class="input-group">-->
        <!--                        <input class="form-control" type="text" name="searchkey" placeholder="Search for.."/>-->
        <!--                        <input class="btn btn-success" type="submit" name="btn" value="Search"/>-->
        <!--                    </div>-->
        <!--                </form>-->
    </div>
</nav>