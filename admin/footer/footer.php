<footer class="py-3 bg-primary">
    <div class="container text-center text-white">
        <h3>BlogSite</h3>
        <p class="m-0 text-white">Copyright &copy; 2020</p>
        <h6>Created By: <span class="text-warning">Toufique Hasan</span></h6>
        <button class="btn btn-warning" data-toggle="modal" data-target="#myModal">Contact</button>
    </div>
    <!-- /.container -->
</footer>

<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Contact</h5>
                <button class="close" data-toggle="modal" data-target="#myModal"><span>&times;</span></button>
            </div>
            <div class="modal-body">
<!--                <div class="card">-->
<!--                    <div class="card-body">-->
                        <form action="" method="POST">
                            <div class="row form-group">
                                <label class="col-sm-3" for="name">Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" id="name" name="name"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3" for="email">Email</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="email" id="email" name="email"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3" for="message">Message</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="message" id="message" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">

                                </div>
                                <div class="col-sm-9">
                                    <input class="btn btn-outline-warning" type="submit" name="btn" value="Send Message"/>
                                </div>
                            </div>
                        </form>
            </div>
        </div>
<!--            </div>-->
<!--        </div>-->
    </div>
</div>