<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 6:16 PM
 */
require_once "../vendor/autoload.php";
include_once "session-check/checkId.php";

$blog = new \App\classes\Blog();
$queryResult = $blog->getBlogs();

if(isset($_GET['delete'])){
    $id = $_GET['blog_id'];
    $message = $blog->deleteBlogById($id);
}
if(isset($_GET['view'])){
    $blogInfo = $blog->getBlogById($_GET['blog_id']);
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Manage category</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>

<?php include_once "navbar/nav-menu.php"; ?>

<?php if(isset($blogInfo)) { ?>
    <div class="jumbotron">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="card">
                    <img class="card-img" src="<?php echo $blogInfo['blog_image']; ?>" alt="<?php echo $blogInfo['blog_image'] ?>">
                    <div class="card-body bg-light">
                        <h3 class="card-title"><?php echo $blogInfo['blog_title']; ?></h3>
                        <hr>
                        <p class="card-text mb-3 text-justify lead"><?php echo $blogInfo['short_description']; ?></p>
                        <hr>
                        <p class="card-text text-justify"><?php echo $blogInfo['long_description']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>

<section id="table" class="my-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12">
<!--                    <div class="card">-->
<!--                        <div class="card-body">-->

                            <?php include_once "alert-message/alert-success.php"; ?>

                            <table class="table table-responsive-xl table-hover table-bordered table-striped">
                                <thead class="bg-primary text-light">
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th class="w-25">Blog Title</th>
<!--                                    <th class="w-25">Long Des.</th>-->
                                    <th>Blog img</th>
                                    <th>Publication Statsus</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                <?php $count = 1; while ($blogInfo = mysqli_fetch_assoc($queryResult)) { ?>
                                    <tr>
                                        <th><?php echo $count++; ?></th>
                                        <td><?php echo $blogInfo['category_name']; ?></td>
                                        <td class="text-justify"><?php echo $blogInfo['blog_title']; ?></td>
<!--                                        <td>--><?php //echo $blogInfo['long_description'] ?><!--</td>-->
								<td>
                                    <img src="<?php echo $blogInfo['blog_image']; ?>" alt="<?php echo $blogInfo['blog_image']; ?>" width="60px">
                                </td>
										<td><?php echo $blogInfo['publication_status'] ? "Published" : "Unpublished"; ?></td>
                                        <td>
                                            <a class="btn btn-outline-success btn-sm" href="?view=true&blog_id=<?php echo $blogInfo['id']; ?>">View</a>
                                            <a class="btn btn-outline-primary btn-sm" href="update-blog.php?blog_id=<?php echo $blogInfo['id']; ?>">Update</a>
                                            <a class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure to delete blog?');" href="?delete=true&blog_id=<?php echo $blogInfo['id']; ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
</section>
<?php include_once "footer/footer.php"; ?>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>