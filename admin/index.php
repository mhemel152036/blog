<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 4:28 PM
 */
session_start();
require_once "../vendor/autoload.php";

$login = new \App\classes\Login();
if  (isset($_SESSION['id'])){
    header('Location: dashboard.php');
}
if(isset($_POST['btn'])) {
    $message = $login->adminLoginCheck($_POST);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Login</title>
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="../assets/css/style1.css"/>
    </head>
    <body>
        <div class="container margin-top">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    <h3 class="text-center">Admin Login</h3>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if(isset($message)) { ?>
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                                            <strong class="alert-">Error! </strong> <?php echo $message; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <form action="" method="POST">
                                <div class="row form-group">
                                    <label class="col-sm-12 col-md-3" for="email">Email</label>
                                    <div class="col-sm-12 col-md-9">
                                        <input class="form-control" type="email" name="email" id="email" placeholder="jhon@gmail.com"/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-12 col-md-3" for="password">Password</label>
                                    <div class="col-sm-12 col-md-9">
                                        <input class="form-control" type="password" name="password" id="password" placeholder="*****"/>
                                        <?php if(isset($message)){ ?>
                                            <a href="">
                                                <span>Forget password?</span>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
<!--                                <div class="row">-->
<!--                                    <div class="col-sm-3"></div>-->
<!--                                    <div class="col-sm-9">-->
<!--                                        -->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3"></div>
                                    <div class="col-sm-12 col-md-9">
                                        <input class="btn btn-success" type="submit" name="btn" value="Login"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
