<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 6/23/2020
 * Time: 5:34 PM
 */
require_once "../vendor/autoload.php";
include_once "session-check/checkId.php";

$category = new \App\classes\Category();

if (isset($_POST['btn'])) {
   $message = $category->addCategory($_POST);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Add category</title>
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="../assets/css/style1.css"/>
    </head>
    <body>

    <?php include "navbar/nav-menu.php"; ?>

    <section id="addCategorySection" ">
        <div class="jumbotron">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-10 col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-center mt-0 mb-4">Add Category</h4>
                                    </div>
                                </div>
                                <?php include_once "alert-message/alert-success.php"; ?>

                                <form action="" method="post">
                                    <div class="row form-group">
                                        <label class="col-sm-12 col-lg-4 col-xl-3" for="category-name">Category Name</label>
                                        <div class="col-sm-12 col-lg-8 col-xl-9">
                                            <input class="form-control" type="text" name="category_name" id="category-name"/>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-12 col-lg-4 col-xl-3" for="category-description">Category Description</label>
                                        <div class="col-sm-12 col-lg-8 col-xl-9">
                                            <textarea class="form-control" name="category_description" id="category-description" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-12 col-lg-4 col-xl-3">Publication status</label>
                                        <div class="col-sm-12 col-lg-8 col-xl-9">
                                            <input class="mr-1" type="radio" name="publication_status" value="1">Published
                                            <input class="mr-1" type="radio" name="publication_status" value="0">Unpublished
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-12 col-lg-4 col-xl-3"></div>
                                        <div class="col-sm-12 col-lg-8 col-xl-9">
                                            <input class="btn btn-success btn-block" type="submit" name="btn" value="Add-Category">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include_once "footer/footer.php"; ?>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/plugin/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({ selector:'textarea' });
    </script>
    <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>